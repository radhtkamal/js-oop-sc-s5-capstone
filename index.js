class Cart {
  constructor() {
    this.contents = [];
    this.totalAmount = 0;
  }

  addToCart(productName, productQty) {
    let newOrder = {
        product: {
          name: productName.name,
          price: productName.price,
          isActive: productName.isActive
        },
        quantity: productQty
    }

    this.contents.push(newOrder);
    return this;
  }

  showCartContents() {
    console.log(this.contents);
  }

  updateProductQuantity(name, newQuantity) {
    this.contents.forEach(item => {
      if (item.product.name == name) {
        item.quantity = newQuantity;
      }
    });
    return this;
  }

  clearCartContents() {
    this.contents = [];
    return this;
  }

  computeCartTotal() {
    let calculator = 0;
    this.contents.forEach(order => {
      let subTotal = order.product.price * order.quantity;
      calculator += subTotal;
    })
    this.totalAmount = calculator;
    return this;
  }
}

class Customer {
  constructor(name, email) {
    this.name = name;
    this.email = email;
    this.cart = new Cart();
    this.orders = [];
  }

  checkOut() {
    this.orders.push(this.cart);
    return this;
  }
}

const radh = new Customer('Radh', 'radh@mail.com');
console.log(radh);

class Product {
  constructor(name, price, isActive) {
    this.name = name;
    this.price = price;
    this.isActive = isActive;
  }

  archive() {
    this.isActive = false;
    return this;
  }

  updatePrice(newPrice) {
    this.price = newPrice;
    return this;
  }
}

const milkTea = new Product('Milk Tea', 75.50, true);
console.log(`Milk tea before updating price: ${milkTea.price}`);
milkTea.updatePrice(95.5);
console.log(`Milk tea after updating price: ${milkTea.price}`);

radh.cart.addToCart(milkTea, 5);
radh.cart.showCartContents();
radh.cart.updateProductQuantity('Milk Tea', 4);
// radh.cart.clearCartContents();
radh.cart.computeCartTotal();
